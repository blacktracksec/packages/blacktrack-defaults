# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/blacktrack-default/issues
# Bug-Submit: https://github.com/<user>/blacktrack-default/issues/new
# Changelog: https://github.com/<user>/blacktrack-default/blob/master/CHANGES
# Documentation: https://github.com/<user>/blacktrack-default/wiki
# Repository-Browse: https://github.com/<user>/blacktrack-default
# Repository: https://github.com/<user>/blacktrack-default.git
